package database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.pj.mynotes.Note;

import java.util.Date;
import java.util.UUID;

import database.SchemaTable.TABLE_NOTE;

public class NoteCursorWaper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public NoteCursorWaper(Cursor cursor) {
        super(cursor);
    }

    public Note getNote(){
        String uuid = getString(getColumnIndex(TABLE_NOTE.COLS.UUID));
        long date = getLong(getColumnIndex(TABLE_NOTE.COLS.DATA));
        String text = getString(getColumnIndex(TABLE_NOTE.COLS.TEXT_NOTE));

        Note note = new Note(UUID.fromString(uuid));
        note.setDate(new Date(date));
        note.setText(text);

        return note;
    }
}
