package database;

public class SchemaTable {

    public final class TABLE_NOTE {
        public static final String TABLE_NAME = "note";

        public final class COLS {
            public static final String UUID = "uuid";
            public static final String DATA = "data";
            public static final String TEXT_NOTE = "textNote";
        }
    }
}
