package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import database.SchemaTable.TABLE_NOTE;

public class SQLHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "DBNote";
    public static final int DB_VERSION = 1;




    public SQLHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+ TABLE_NOTE.TABLE_NAME+ "("
                +  "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TABLE_NOTE.COLS.UUID + ", "
                + TABLE_NOTE.COLS.DATA + ", "
                + TABLE_NOTE.COLS.TEXT_NOTE + " )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}