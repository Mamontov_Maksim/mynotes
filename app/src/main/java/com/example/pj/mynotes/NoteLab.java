package com.example.pj.mynotes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import database.NoteCursorWaper;
import database.SQLHelper;
import database.SchemaTable;
import database.SchemaTable.TABLE_NOTE;

public class NoteLab {
    private static NoteLab mNoteLab;

    private Context mContext;
    private SQLiteDatabase mDataBase;


    private NoteLab(Context context){
        mContext = context.getApplicationContext();
        mDataBase = new SQLHelper(mContext).getWritableDatabase();
    }

    public static NoteLab getNoteLab(Context context){
        if (mNoteLab == null){
            mNoteLab = new NoteLab(context);
        }
        return mNoteLab;
    }

    public void updateNote (Note note){
        String uuid = note.getId().toString();
        ContentValues contentValues = getContent(note);
        mDataBase.update(TABLE_NOTE.TABLE_NAME, contentValues,
                TABLE_NOTE.COLS.UUID + " = ?",
                new String[] {uuid});
    }


    public void addNote(Note note){
        ContentValues contentValues = getContent(note);
        mDataBase.insert(TABLE_NOTE.TABLE_NAME, null, contentValues);
    }

    public void deletedNote(UUID uuid){
        mDataBase.delete(TABLE_NOTE.TABLE_NAME, TABLE_NOTE.COLS.UUID + " = ?",
                        new String[]{uuid.toString()});
    }

    public Note getNote(UUID idNote) {
        NoteCursorWaper cursor = query(TABLE_NOTE.COLS.UUID + " = ?",
                                       new String[]{idNote.toString()});
        try {
            if (cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            return cursor.getNote();
        } finally {
            cursor.close();
        }
    }

    public List<Note> getListNote(){
        List<Note> notes = new ArrayList<>();
        NoteCursorWaper cursor = query(null,null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                notes.add(cursor.getNote());
                cursor.moveToNext();
            }
            return notes;
        } finally {
            cursor.close();
        }
    }

    private ContentValues getContent(Note note){
        ContentValues values = new ContentValues();
        values.put(TABLE_NOTE.COLS.UUID, note.getId().toString());
        values.put(TABLE_NOTE.COLS.DATA, note.getDate().getTime());
        values.put(TABLE_NOTE.COLS.TEXT_NOTE, note.getText());
        return values;
    }

    private NoteCursorWaper query(String whereClause, String[] whereArgs){
        Cursor cursor = mDataBase.query(TABLE_NOTE.TABLE_NAME, null, whereClause,
                whereArgs, null, null, null);
        return new NoteCursorWaper(cursor);


    }
}
