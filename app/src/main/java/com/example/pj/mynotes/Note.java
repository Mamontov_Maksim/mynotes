package com.example.pj.mynotes;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Note {

    private Date mDate;
    private UUID mId;
    private String mText;

    public Note() {
        this(UUID.randomUUID());
    }

    public Note(UUID id) {
            mId = id;
            mDate = new Date();
    }

    class myDate extends Date{
        @Override
        public String toString() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE HH:mm, dd.MM.y");
            return simpleDateFormat.format(mDate);

        }
    }

    public Date getDate () {
        return new myDate();
    }

    public void setDate (Date mDate){
        this.mDate = mDate;
    }

    public UUID getId () {
        return mId;
    }

    public void setId (UUID mId){
        this.mId = mId;
    }

    public String getText () {
        return mText;
    }

    public void setText (String mText){
        this.mText = mText;
    }
}
