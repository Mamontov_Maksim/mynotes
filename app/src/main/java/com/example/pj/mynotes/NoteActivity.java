package com.example.pj.mynotes;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.UUID;


public class NoteActivity extends SingleFragmentActivity{

    public static final String EXTRA_NOTE_ID = "note_id";

    private Note mNote;

    public static Intent newIntent(Context context, UUID idNote){
        Intent intent = new Intent(context, NoteActivity.class);
        intent.putExtra(EXTRA_NOTE_ID, idNote);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        UUID uuid =(UUID) this.getIntent().getSerializableExtra(EXTRA_NOTE_ID);
        mNote = NoteLab.getNoteLab(this).getNote(uuid);
        return NoteFragment.init(mNote.getId());
    }
}
