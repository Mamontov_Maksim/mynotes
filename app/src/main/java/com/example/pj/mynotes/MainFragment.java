package com.example.pj.mynotes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class MainFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private MainAdapter mAdapter;


    public static MainFragment init(){
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_main);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_note_list, menu);
    }

    private class MainHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener{
        private Button mFirstLatterButton;
        private TextView mFirstText;
        private TextView mDateText;

        private Note mNote;

        public MainHolder(LayoutInflater layoutInflater, ViewGroup container) {
            super(layoutInflater.inflate(R.layout.list_item_main, container, false));
            itemView.setOnClickListener(this);
            mFirstLatterButton = itemView.findViewById(R.id.first_latter_text_button);
            mFirstLatterButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    dialogBuilder.setMessage("Вы точно хотите удалить запись ?")
                            .setCancelable(false)
                            .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    NoteLab.getNoteLab(getActivity()).deletedNote(mNote.getId());
                                    onResume();
                                    dialog.cancel();
                                }
                            })
                            .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();

                                }
                            });
                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.setTitle("Удаление записи");
                    alertDialog.show();

                }
            });
            mFirstText = itemView.findViewById(R.id.first_records_text);
            mDateText = itemView.findViewById(R.id.date_text);
        }

        public void bind(Note note){
            mNote = note;
            String latterOnButton = "(>.<)";
            if (mNote.getText() != null){
                latterOnButton = mNote.getText().toUpperCase().trim().charAt(0) + "";
            }
            mFirstLatterButton.setText(latterOnButton);
            mFirstText.setText(formattingText());
            mDateText.setText(mNote.getDate().toString());

        }

        private String formattingText(){
            if (mNote.getText() == null){
                return "";
            }
            String text = mNote.getText().trim();
            if (text.length() >= 40){
                char[] charArray = new char[40];
                for (int i = 0; i < charArray.length; i++){
                    charArray[i] = text.charAt(i);
                }
                text = new String(charArray);
                text = text.trim();
                text += "...";
            }
            return text;
        }

        @Override
        public void onClick(View v) {
            Intent intent = NoteActivity.newIntent(getActivity(), mNote.getId());
            startActivity(intent);
        }
    }

    private class MainAdapter extends RecyclerView.Adapter<MainHolder>{

        List<Note> mNotes;

        public MainAdapter(List<Note> notes){
            mNotes = notes;
        }

        @NonNull
        @Override
        public MainHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new MainHolder(layoutInflater, viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull MainHolder mainHolder, int i) {
            Note note = mNotes.get(i);
            mainHolder.bind(note);
        }

        @Override
        public int getItemCount() {
            return mNotes.size();
        }

        public void setNotes(List<Note> notes){
            mNotes = notes;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_note:
                Note note = new Note();
                NoteLab.getNoteLab(getActivity()).addNote(note);
                Intent intent = NoteActivity.newIntent(getActivity(), note.getId());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateUI(){
        NoteLab noteLab = NoteLab.getNoteLab(getActivity());
        List<Note> notes = noteLab.getListNote();

        if (mAdapter == null){
            mAdapter = new MainAdapter(notes);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setNotes(notes);
            mAdapter.notifyDataSetChanged();
        }
    }
}
