package com.example.pj.mynotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;
import java.util.UUID;

public class NoteFragment extends Fragment {

    private static final String ARGS_ID = "NoteFragment_note_id";

    private EditText mDateText;
    private EditText mEditTextNote;

    private Note mNote;

    public static NoteFragment init(UUID uuid){
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARGS_ID, uuid);
        NoteFragment noteFragment = new NoteFragment();
        noteFragment.setArguments(bundle);
        return noteFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UUID idNote = (UUID) getArguments().getSerializable(ARGS_ID);
        mNote = NoteLab.getNoteLab(getActivity()).getNote(idNote);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);

        mDateText = (EditText) view.findViewById(R.id.date_text_note);
        mDateText.setText(mNote.getDate().toString());
        mEditTextNote = (EditText) view.findViewById(R.id.edit_text_note);
        mEditTextNote.setText(mNote.getText());
        mEditTextNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    mNote.setText(null);
                    return;
                }
                mNote.setText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        mNote.setDate(new Date());
        NoteLab.getNoteLab(getActivity()).updateNote(mNote);
    }
}
